var searchData=
[
  ['calibrategrippers',['CalibrateGrippers',['../class_you_bot_1_1_driver.html#ac27812c39e28781807b1203c2d8dcbff',1,'YouBot::Driver']]],
  ['carterrbelowthresh',['CartErrBelowThresh',['../class_c_t_r_l_1_1_multi_arm.html#a0198673a1f912e6412e145d20263826f',1,'CTRL::MultiArm']]],
  ['clone',['clone',['../class_c_t_r_l_1_1_arm_model.html#ab6fa8374cf887fdd5d88fe64d440cc4c',1,'CTRL::ArmModel::clone()'],['../class_c_t_r_l_1_1_vehicle_model.html#ad20d0047888f4c16b512c46941e7d0b6',1,'CTRL::VehicleModel::clone()']]],
  ['closegripper',['CloseGripper',['../class_c_t_r_l_1_1_controller_interface.html#a52ab6882c7d474bff3bd20fa9ce807e6',1,'CTRL::ControllerInterface::CloseGripper()'],['../class_you_bot_1_1_driver.html#a095a96f0d6b21e8622b1e002199cae60',1,'YouBot::Driver::CloseGripper()']]],
  ['compute',['Compute',['../class_c_t_r_l_1_1_virtual_frame.html#a68d494564581aa8d835cabbb3bd69882',1,'CTRL::VirtualFrame::Compute(const CMAT::TransfMatrix &amp;wTt, const CMAT::TransfMatrix &amp;wTg, CMAT::TransfMatrix &amp;wTv)'],['../class_c_t_r_l_1_1_virtual_frame.html#ab7c2b6d755d51ddfe33c53122e6fff5c',1,'CTRL::VirtualFrame::Compute(const CMAT::TransfMatrix &amp;wTt, const CMAT::Vect6 &amp;xdotbar, CMAT::TransfMatrix &amp;wTv)'],['../class_c_t_r_l_1_1_acceleration_filter.html#af32d7f0cf77e78b6692d59f0766a7eb0',1,'CTRL::AccelerationFilter::Compute(const CMAT::Matrix &amp;u, CMAT::Matrix &amp;y)'],['../class_c_t_r_l_1_1_acceleration_filter.html#a067ded3b86e19eb3bca703478fb4cb81',1,'CTRL::AccelerationFilter::Compute(const double u, double &amp;y)']]],
  ['computecontrol',['ComputeControl',['../class_c_t_r_l_1_1_multi_arm.html#a0b3785e48e7c4a3092a4cdf98f03cc31',1,'CTRL::MultiArm']]],
  ['computejointcontrol',['ComputeJointControl',['../class_c_t_r_l_1_1_multi_arm.html#a5ee5d99d4727b62d26a447746957a07a',1,'CTRL::MultiArm::ComputeJointControl()'],['../class_c_t_r_l_1_1_single_arm.html#a1fd5fc098bb8c72509af5620a4cba3e7',1,'CTRL::SingleArm::ComputeJointControl()']]],
  ['computey',['ComputeY',['../class_c_t_r_l_1_1_task_priority.html#ac01fdb4884692458286c04b7aa749556',1,'CTRL::TaskPriority']]],
  ['computeybiman',['ComputeYBiman',['../class_c_t_r_l_1_1_task_priority.html#ae6920e569eb35daeccb92ec7b6b382bc',1,'CTRL::TaskPriority']]],
  ['confirmmodedialog',['ConfirmModeDialog',['../class_you_bot_1_1_driver.html#ae6749e4200edd2cb0609808879a36090',1,'YouBot::Driver']]]
];
