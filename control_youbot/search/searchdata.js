var indexSectionsWithContent =
{
  0: "abcdefghijlmnoprstuvwy~",
  1: "acdfgmprstuv",
  2: "o",
  3: "cy",
  4: "acdefgilnoprstuvw~",
  5: "bcemw",
  6: "ct",
  7: "bcdefhijmorstu",
  8: "s",
  9: "dy"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Pages"
};

