var indexSectionsWithContent =
{
  0: "abcdefgijlmnprstvw~",
  1: "abenprtvw",
  2: "er",
  3: "aefmnprstv",
  4: "abcdefgilrsv~",
  5: "abcdfijlmrtv",
  6: "j",
  7: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Pages"
};

