var searchData=
[
  ['reducedversorlemma',['ReducedVersorLemma',['../namespacerml.html#ab20f3c0c8768dcec23905161150028dc',1,'rml']]],
  ['regularizedpseudoinverse',['RegularizedPseudoInverse',['../namespacerml.html#aa1be14a3d46c7ac56a8daefebf338ded',1,'rml']]],
  ['robotmodel',['RobotModel',['../classrml_1_1_robot_model.html#af7a77200c7dfff02c3a1433e6b1a7fa0',1,'rml::RobotModel::RobotModel(Eigen::TransfMatrix bodyFrame, std::string frameID)'],['../classrml_1_1_robot_model.html#a497f450461b99090285736292f2ea713',1,'rml::RobotModel::RobotModel(Eigen::TransfMatrix bodyFrame, std::string frameID, Eigen::MatrixXd JBodyFrame)']]]
];
